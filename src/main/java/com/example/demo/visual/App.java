package com.example.demo.visual;

import com.example.demo.dominio.EstudianteRepository;
import com.example.demo.dominio.Libro;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.Button;
import com.vaadin.ui.TextField;
import com.vaadin.ui.components.grid.FooterRow;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.*;

/**
 * Created by Guicho on 15/07/2017.
 */
@SpringUI
public class App extends UI {
    @Autowired
    EstudianteRepository estudianteRepository;

//    Button alu = new Button("Ingresar Alumnos");
  //  Button edit = new Button("Ingresar Editorial");
    @Override
    protected void init (VaadinRequest vaadinRequest){
        VerticalLayout Vlayout = new VerticalLayout();
        //HorizontalLayout vlayout = new HorizontalLayout();
        TextField nombre = new TextField("Nombre");
        TextField autor = new TextField("Autor");
        TextField editorial =new TextField("Editorial");
        TextField curso =new TextField("Curso");


        Grid<Libro> grid = new Grid<>();
        grid.addColumn(Libro::getId).setCaption("ID");
        grid.addColumn(Libro::getNombreL).setCaption("Nombre");
        grid.addColumn(Libro::getAutor).setCaption("Autor");
        grid.addColumn(Libro::getEditorial).setCaption("Editorial");
        grid.addColumn(Libro::getCurso).setCaption("Curso");


        Button add = new Button("Ingresar Libro");
        add.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Libro e = new Libro();
                e.setNombreL(nombre.getValue());
                e.setAutor(autor.getValue());
                e.setEditorial(editorial.getValue());
                e.setCurso(curso.getValue());
                //e.setEdad(Integer.parseInt(edad.getValue()));

                estudianteRepository.save(e);
                grid.setItems(estudianteRepository.findAll());

                nombre.clear();
                autor.clear();
                editorial.clear();
                curso.clear();
            }
        });

        Button rem = new Button("Eliminar Libro");


        Vlayout.addComponentsAndExpand(nombre,autor,editorial,curso,add,rem);

        //Vlayout.addComponentsAndExpand(Vlayout);
        //Vlayout.addComponentsAndExpand(grid);
        Vlayout.addComponent(grid );
        new Dimension(50,20);
        //hlayout.setComponentAlignment(add, Alignment.BOTTOM_LEFT);

        setContent(Vlayout);
    }
}
