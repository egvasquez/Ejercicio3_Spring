package com.example.demo.dominio;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by egust on 20/07/2017.
 */
@Entity

public class Libro {
    private String nombreL;
    private String autor;
    private String editorial;
    private String curso;

    @Id@GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    public Long getId() {return id;}

    public void setId(Long id) {this.id = id;}


    public Libro() {
    }

    public Libro(String nombreL, String autor, String editorial, String curso) {
        this.nombreL = nombreL;
        this.autor = autor;
        this.editorial = editorial;
        this.curso = curso;
    }

    public String getNombreL() {
        return nombreL;
    }

    public void setNombreL(String nombreL) {
        this.nombreL = nombreL;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }
}
