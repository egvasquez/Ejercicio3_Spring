package com.example.demo.dominio;

/**
 * Created by egust on 21/07/2017.
 */
public class Editorial {
    private String nombreE;
    private String direccion;

    public Editorial() {
    }

    public Editorial(String nombreE, String direccion) {
        this.nombreE = nombreE;
        this.direccion = direccion;
    }

    public String getNombreE() {
        return nombreE;
    }

    public void setNombreE(String nombreE) {
        this.nombreE = nombreE;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
