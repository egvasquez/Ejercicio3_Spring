package com.example.demo.dominio;

/**
 * Created by egust on 21/07/2017.
 */
public class Alumnos {
    private String nombreA;
    private String edad;
    private String Direccion;

    public Alumnos() {
    }

    public Alumnos(String nombreA, String edad, String direccion) {
        this.nombreA = nombreA;
        this.edad = edad;
        Direccion = direccion;
    }

    public String getNombreA() {
        return nombreA;
    }

    public void setNombreA(String nombreA) {
        this.nombreA = nombreA;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }
}
